var canvas = document.getElementById('canvas')
var context = canvas.getContext('2d')
var img = new Image()
var pixelWidth = 1
var pixelHeight = 1
var shouldRedraw = true


var data
var imageWidth
var imageHeight

// fill screen
var winWidth = window.innerWidth
var winHeight = window.innerHeight


// setup
function setup() {
  imageWidth = canvas.width = img.width
  imageHeight = canvas.height = img.height
  canvas.style.marginLeft = '-' + imageWidth / 2 + 'px'
  canvas.style.marginTop = '-' + imageHeight / 2 + 'px'
  canvas.style.border = '1px solid #333'
  context.drawImage(img, 0, 0)
  data = context.getImageData(0, 0, img.width, img.height).data
  window.onmousemove = onMouseMove
  draw()
}

// onMouseMove
function onMouseMove(ev) {
  var amt, pw, ph
  amt = Math.abs(Math.floor(winHeight / 2 - ev.y))
  amt = Math.max(amt, 1)
  pw = Math.max(Math.floor(imageWidth / amt), 1)
  ph = Math.max(Math.floor(imageHeight / amt), 1)
  if (pw != pixelWidth || ph != pixelHeight) {
    pixelWidth = pw
    pixelHeight = ph
    shouldRedraw = true
  }
}

// draw
function draw() {
  if (!shouldRedraw) {
    requestAnimationFrame(draw)
    return
  }
  context.clearRect(0, 0, winWidth, winHeight)
  var y, x, red, green, blue, p
  for(y = 0; y < imageHeight; y += pixelHeight) {
    for(x = 0; x < imageWidth; x += pixelWidth) {
      p = ((imageWidth * y) + x) * 4
      red = data[p]
      green = data[p + 1]
      blue = data[p + 2]
      context.fillStyle = 'rgb(' + red + ', ' + green + ', ' + blue + ')'
      context.fillRect(x, y, pixelWidth, pixelHeight)
    }
  }
  shouldRedraw = false
  requestAnimationFrame(draw)
}


// load image
img.onload = setup
img.src = '/moonwalk.jpg'
